@extends("templates.master")
@isset($annonce)
        @section("titre", "Détails de l'annonce ".$annonce->titre)
    @endisset
@section("contenu")
    @isset($annonce)
<div style="text-align:center">
<h2 class="mt-3 mb-2">Détails de l'annonce numéro {{$annonce->id}} </h2>
              @if(!empty($annonce->photo))
                        <img src="{{asset($annonce->photo)}}" alt="photo" width="40%"/> <br>
                    @else
                        <img src="{{asset('photo_annonce/no-image.jpg')}}" alt="no-image"  width="40%"/><br> 
                    @endif
            <b>Titre : </b> {{$annonce->titre}} <br>
            <b>Description : </b> {{$annonce->description}} <br>
            <b>Type : </b> {{$annonce->type}} <br>
            <b>Etat : </b> {{$annonce->neuf? "Neuf" : "Ancien"}} <br>
            <b>Superficie : </b> {{$annonce->superficie}} <br>
            <b>Prix : </b> {{$annonce->prix}} <br>
       @endisset 
</div>
            
@endsection