<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AnnonceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    { 
       
        return [
            "titre"=>["required",
                        Rule::unique("annonces")->ignore($this->annonce)],
            "description"=>"max:100",
            "type"=>"in:Appartement,Maison,Villa,Magasin,Terrain",
            "ville"=>"alpha",
            "superficie"=>"integer|min:20",
            "prix"=>"numeric|min:0",
            "photo"=>"image"
        ];
    }
     public function messages(): array
    { 
       
        return [
            "titre.required"=>"Vous devez définir un titre",
            "titre.unique"=>"Veuillez saisir un autre titre (Ce titre existe déjà)",
              "description.max"=>"La logueur maximale d'une description est définie à 100",
            "type.in"=>"Le type peut prendre l'une des valeurs suivantes: Appartement, Maison, Villa, Magasin et Terrain",
            "ville.alpha"=>"la ville ne peut contenir que des caractères alphabétiques",
            "superficie.min"=>"La superficie minimale est 20",
            "prix.numeric"=>"Le prix doit être numérique",
            "prix.min"=>"Le prix doit être positif",
            "photo.image"=>"La photo de l'annonce doit être une image"

        ];

    }
}
