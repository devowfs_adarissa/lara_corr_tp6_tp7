<?php

namespace App\Http\Controllers;

use App\Models\Annonce;
use App\Http\Requests\AnnonceRequest;
use Illuminate\Http\Request;

class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $annonces=Annonce::all();
       return view("annonces.index", compact("annonces"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("annonces.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AnnonceRequest $request)
    {
       
        $file=$request->photo;
         //Il faut tout d'abord configurer, sous app/config/filesystems, le lien (link): photo_annonce
        $path= $file->store("photo_annonce");
        //Avec la méthode store, on laisse à laravel de nous générer un identifiant unique,
        //La méthode store retourne le chemin avec le nom du fichier généré; ex. : photo_annonce/ioCAfg6lgV6zFneg08F5OdxaStk20ezjsXE3g3f6.jpg
        //la valeur retournée est stockée par la suite dans la variable $path;
        //On va utiliser $path pour enregister ce chemin dans la base de données
        //Vous pouvez utiliser la méthode storeAs (voir exemple de cours) si vous voulez personnaliser le nom de vos fichiers

                Annonce::create(
            [
                "titre"=>$request->titre,
                "description"=>$request->description,
                "type"=>$request->type,
                "neuf"=>$request->neuf,
                "ville"=>$request->ville,
                "superficie"=>$request->superficie,
                "prix"=>$request->prix,
                "photo"=>$path      
            ]
        );
    return redirect()->route("annonce.index")->with("success", "la nouvelle annonce a été bien ajouté!");
    }

    /**
     * Display the specified resource.
     */
    public function show(Annonce $annonce)
    {
       return view("annonces.show", compact("annonce"));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Annonce $annonce)
    {
        return view("annonces.edit" , compact("annonce"));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(AnnonceRequest $request, Annonce $annonce)
    {

        //Si on veut configurer les validations ici, on peut utiliser le code suivant pour définir la contarainte du titre unique:
        // $request->validate( [
        //  "titre"=> "required|unique:annonces,titre,".$annonce->id
        // ]);

            //Si on ne veut pas faire une affectation champ par champ on peut utiliser $request->all()
            $data=$request->all();

             $file=$request->photo;
             if(isset($file)){
                $path= $file->store("photo_annonce");
                //avant d'exécuter la mise à jour, il faut modifier le champ "photo" pour qu'il reçoive le nouveau path 
                $data['photo']=$path;
             }else{
                 $data['photo']=$annonce->photo;
             }
             
            

            $annonce->update($data);
         return redirect()->route("annonce.index")->with("success", "L'annonce a été mise à jour!");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Annonce $annonce)
    {
       $annonce->delete();
       return redirect()->route("annonce.index")->with("success", "L'annonce a été supprimé!");

    }
}
